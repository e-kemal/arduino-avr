/*
 */

#include <avr/io.h>
#include <util/delay.h>

int main(void)
{
    //OC0B, пин 3 ардуины
    DDRD = 0x01;

    //COM0B = 01 - переключение пина при совпадении
    //WGM0 = 10 - режим CTC (сброс при совпадении)
    TCCR0A = (0 << COM0B1) | (1 << COM0B0) | (1 << WGM01) | (0 << WGM00);

    //CS0 = 000 изначально таймер выключен
    TCCR0B = (0 << CS02) | (0 << CS01) | (0 << CS00);

    //REFS = 11 - используем внутренний источник опорного напряжения
    //ADLAR - используем левое выравнивание
    //MUX = 00111 - используем канал 7 (ВНЕЗАПНО аналоговые пины мапятся в обратно порядке)
    ADMUX = (1 << REFS1) | (1 << REFS0) | (1 << ADLAR) | (0 << MUX4) | (0 << MUX3) | (1 << MUX2) | (1 << MUX1) | (1 << MUX0);

    //ADEN - разрешаем работу АЦП
    ADCSRA = (1 << ADEN);

    while(1){
        //запускаем преобразование
        ADCSRA |= (1 << ADSC);
        //ожидаем завершения преобразования
        while (ADCSRA & (1 << ADSC));
        //читаем младшую часть
        uint16_t rotation = ADCL;
        //читаем старшую часть
        rotation |= (ADCH << 8);

        //Пересчитываем значение АЦП в частоту
        uint32_t freq = rotation * 1000. / 1023. + 3500;

        //Пытаемся вычислить делитель (значение регистра сравнения)
        uint32_t ocr = F_CPU / freq / 2 - 1;
        //если умещаемся в разрядность счётчика
        if (ocr <= 255) {
            //то запускаем его без предделителя
            TCCR0B = 0b00000001;
        }
        else {
            //иначе снова вычисляем делитель с учётом следующей ступени предделения
            ocr = F_CPU / freq / 2 / 8 - 1;
            if (ocr <= 255) {
                TCCR0B = 0b00000010;
            }
            else {
                ocr = F_CPU / freq / 2 / 64 - 1;
                if (ocr <= 255) {
                    TCCR0B = 0b00000011;
                }
                else {
                    ocr = F_CPU / freq / 2 / 256 - 1;
                    if (ocr <= 255) {
                        TCCR0B = 0b00000100;
                    }
                    else {
                        ocr = F_CPU / freq / 2 / 1024 - 1;
                        if (ocr <= 255) {
                            TCCR0B = 0b00000101;
                        }
                        else {
                            TCCR0B = 0;
                        }
                    }
                }
            }
        }
        //пишем вычисленное значение в регистр сравнения
        OCR0A = ocr;

        _delay_ms(20);
    }

    return 0;
}
